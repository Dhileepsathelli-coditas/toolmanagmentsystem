
class Http {
    #baseUrl = "https://a62d-103-176-135-84.in.ngrok.io";
    // #baseUrl = "https://e18b-103-169-241-128.in.ngrok.io";
    async send(endpoint, token, options = {}, bodyData = null) {
        try {
            options = {
                ...options,
                headers: {
                    "ngrok-skip-browser-warning": "1234",
                    'Authorization': `Bearer ${token}`,
                },
                body: bodyData ? JSON.stringify(bodyData) : null
            }
            console.log(token);
            const response = await fetch(`${this.#baseUrl}/${endpoint}`, options);
            const responseData = await response.json();

            return responseData;
        } catch (e) {
            throw e;
        }
    }
    async get(endpoint, token) {
        return await this.send(endpoint, token, { method: 'GET' });
    }

    async delete(endpoint) {
        return await this.send(endpoint, { method: 'DELETE' });
    }

    async post(endpoint, bodyData) {
        return await this.send(endpoint, { method: 'POST' }, bodyData)
    }

    async put(endpoint, data) {
        return await this.send(endpoint, { method: 'PUT' }, bodyData)
    }
}

const http = new Http();

export default http;