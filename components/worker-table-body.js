export const workersTableBody=(worker)=>{
    return (`<tr>
    <td>${worker.workerId} </td>
    <td>${worker.workerName} </td>
    <td>${worker.workerUsername} </td>
    <td>${worker.workerSalary} </td>
    <td><button type="button" class="btn btn-primary" id="editBtnWorker">Edit</button></td>
    <td><button type="button" class="btn btn-danger" id="deleteBtnWorker">delete</button></td>

</tr>
    `)
}