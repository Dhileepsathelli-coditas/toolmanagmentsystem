export const toolsTableBody = (tool) => {
    return (`

                        <tr>
                            <td>${tool.toolId} </td>
                            <td>${tool.toolName} </td>
                            <td>${tool.toolSize} </td>
                            <td>${tool.toolPrice} </td>
                            <td><button type="button" class="btn btn-primary" id="editBtnIron">Edit</button></td>
                            <td><button type="button" class="btn btn-danger" id="deleteBtnIron">delete</button></td>
        
                        </tr>

                
    `)
}