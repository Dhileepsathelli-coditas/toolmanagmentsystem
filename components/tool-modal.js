export const ToolModal = () => {
    return (`
    
        
            <div class="modal">
            <h1 class='modal-header'>Create Tool</h1>
            <form action="https://a62d-103-176-135-84.in.ngrok.io/admin/createTool" method='POST' class="form">
            <label for="">Enter the Tool ID</label>
            <input type="number" required>
            <label for="">Enter the Tool Name</label>
            <input type="text" required>
            <label for="">Enter the Tool Size</label>
            <input type="number" required>
            <label for="">Enter the Tool Price</label>
            <input type="number" required>
            <button type="button" class="form-button" id='createToolBtn'>Submit</button>
        </form>
            </div>
        

    `)
}