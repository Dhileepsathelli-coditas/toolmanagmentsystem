export const Dashboard = () => {
    return (`
    <section class="dashboard">

        <section class="button">
            <h2 class="heading">Dashboard</h2>
            <button type="button" class="click ordersBtn" id='ordersButton'>Orders</button>
            <button type="button" class="click workersBtn" id='workersButton'>Workers</button>
            <button type="button" class="click ordersBtn" id='toolsButton'>Tools</button>
        </section>
        <section class="admin-content" id="admin-content">
        </section>

    </section>
    `)
}