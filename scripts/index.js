import { loginForm } from "../components/login-form.js";
import {$} from "../dom/dom.js";
import references from "../references/reference.js"
import http from "../http/http.js";
import { Dashboard } from "../components/admin-dashboard.js";
import { toolsContent } from "../components/tools-table.js";
import { toolsTableBody } from "../components/tools-table-body.js"
import { ToolModal } from "../components/tool-modal.js";
import { workersContent } from "../components/worker-table.js";
import { workersTableBody } from "../components/worker-table-body.js";
import { ordersContent } from "../components/orders-table.js";
import { ordersTableBody } from "../components/orders-table-body.js";


const {mainElement,createModal} = references;
// console.log(mainElement);
const displayLoginPage=()=>{
    mainElement[0].innerHTML = loginForm();
}
displayLoginPage();
const submitBtn=$('#loginBtn');

const loginPage = async(e)=>{
    e.preventDefault();
    const userName = $('#username');
    const password = $('#password')
    const bodyData = {
        "username": `${userName.value}`,
        "password": `${password.value}`
    }
    try{
        
        const response = await fetch('https://a62d-103-176-135-84.in.ngrok.io/authenticate', {
            method: 'POST',
            headers: {
                "ngrok-skip-browser-warning": "1234",
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyData)
        })
        const responseToken = await response.json();
        const token = responseToken.jwtToken;
        const role = responseToken.userRole;
        return displayDasboard(e,token);

    }catch(e){
        console.log("error");
    }
}
submitBtn.addEventListener('click', (e) => loginPage(e));


const displayDasboard=(e,token)=>{
    mainElement[0].innerHTML=Dashboard();
    const tools=$('#toolsButton');
    const workers=$('#workersButton');
    const orders=$('#ordersButton');
    const adminContent=$('#admin-content');
    tools.addEventListener('click', () => displayTools(adminContent,token));
    workers.addEventListener('click', () => workersDisplay(adminContent, token));
    orders.addEventListener('click', () => ordersDisplay(adminContent, token));
}

const displayTools=(adminContent,token)=>{
    adminContent.innerHTML = '';
    adminContent.innerHTML = toolsContent();

    const toolsTable = $('#toolsTbl');
    const CreateButton = $('#createEle');

    const displayToolsTableRow = async () => {
        try {
            const responseTools = await http.get('admin/getTools', token);

            for (let tools of responseTools) {
                const toolsTableRow = document.createElement('tbody');
                toolsTableRow.innerHTML += toolsTableBody(tools);
                toolsTable.appendChild(toolsTableRow);
            }

        } catch (error) {
            throw error;
        }
    }
    displayToolsTableRow();
    CreateButton.addEventListener('click', () => ToolModalDisplay())

}


const ToolModalDisplay=()=>{
    createModal[0].innerHTML=ToolModal();
    const submitcreateToolButton=$('#createToolBtn');
    submitcreateToolButton.addEventListener('click',(e)=>submitTool(e));
}

const submitTool=(e)=>{


}

const workersDisplay=(adminContent,token)=>{
    adminContent.innerHTML='';
    adminContent.innerHTML=workersContent();
    const workers=$('#workersTbl');
    const workersDisplayTableElements=async ()=>{
        try{
            const responseWorkers=await http.get('admin/getWorkers',token);
            for(let Worker of responseWorkers){
                const workersTblRow=document.createElement('tbody');
                workersTblRow.innerHTML+=workersTableBody(Worker);
                workers.appendChild(workersTblRow);
            }

        }catch(e){
            console.log("error")
        }
    }
    
    workersDisplayTableElements();
}

 
const ordersDisplay =(adminContent,token)=>{
    adminContent.innerHTML='';
    adminContent.innerHTML=ordersContent();
    const orders = $('#workersTbl');
    const ordersDisplayTableElements = async () => {
        try {
            const responseOrders = await http.get('admin/getOrders', token);
            console.log(responseOrders);
                for (let orders of responseOrders) {
                    const ordersTableRow = document.createElement('tbody');
                    ordersTableRow.innerHTML += ordersTableBody(orders);
                    orders.appendChild(ordersTableRow);
                    }
                } catch (error) {
                    throw error;
                }
            }
         ordersDisplayTableElements();
}
    



// const displayOrders = (adminContent, token) => {
//     adminContent.innerHTML = '';
//     adminContent.innerHTML = ordersContent();

//     const ordersTable = $('#workersTbl');

//     const displayOrdersTableRow = async () => {
//         try {
//             const responseOrders = await http.get('admin/getOrders', token);
//             console.log(responseOrders);

//             for (let orders of responseOrders) {
//                 const ordersTableRow = document.createElement('tbody');
//                 ordersTableRow.innerHTML += ordersTableBody(orders);
//                 ordersTable.appendChild(ordersTableRow);
//             }
//         } catch (error) {
//             throw error;
//         }
//     }
//     displayOrdersTableRow();
// }













// const root=document.getElementById("root");
// function createElement(tagName,content="",className)
// {
//     const elementName=document.createElement(tagName);
//     elementName.textContent=content;
//     document.getElementById("root").appendChild(elementName);
//     elementName.classList.add(className);
// }
// function createLoginForm(){
//     createElement("h1","Login page","label")
//     const loginPageTemplate=`<form class="login_form" action="" method="POST">
//     <label for="username">Username</label>
//     <input type="text" id="username" placeholder="Enter your Username">
//     <label for="password">Password</label>
//     <input type="text" id="password" placeholder="Enter your password">
//     <button class="btn" id="loginBtn" type="button">submit</button>
//     </form>`
//     root.insertAdjacentHTML('beforeend',loginPageTemplate);
// }
// createLoginForm()







// const submitBtn=document.getElementById("loginBtn");
// submitBtn.addEventListener('click',async(e)=>{
//     e.preventDefault();
//     const data={
//         username:document.querySelector("#username").value,
//         password:document.querySelector("#password").value
//     }
//     console.log(data);


//     const authenticatedResponse= await fetch("http://92d2-103-176-135-84.ngrok.io/authenticate",{
//         method: "POST",
//         body: JSON.stringify(data),
//         headers: new Headers({
//           "ngrok-skip-browser-warning": "1234",
//           'Content-type': 'application/json',
//         //   "Authorization":"Bearer "+this.#token1
//         }),
        
      
//     })   
//     const data1=authenticatedResponse.JSON();
//     console.log(data1)

// })




    
//     // const myForm=document.getElementsByClassName('login_form');
//     // const displaySetting=myForm.style.display;
//     // console.log(displaySetting);
//     // if(displaySetting==='block'){
//     //     myForm.style.display='none';
//     // }else{
//     //     myForm.style.display='block';

//     // }






// // const main=async()=>{
// //     try{






// //     }catch(e){
// //         console.log(e);

// //     }

// // }
// // main();
// // export default {
// //     main
// // }


 








