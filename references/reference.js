import { $ } from '../dom/dom.js';
const mainElement = $('.root');
const htmlBody = $('body');
const h1Content = $('.label');
const createModal=$('.createModal')

export default {
    mainElement,
    htmlBody,
    h1Content,
    createModal
}